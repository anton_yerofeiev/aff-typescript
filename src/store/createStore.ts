import {createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger'
import mySaga from '../containers/Casinos/saga'
import rootReducer from "./rootReducer";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer,
    applyMiddleware(logger, sagaMiddleware)
);

export type StoreType = ReturnType<typeof createStore>

sagaMiddleware.run(mySaga);

export default store;