import { ActionType } from 'typesafe-actions';

import * as actionsCasino from '../containers/Casinos/actions';
import casinoReducer, {Casinos} from '../containers/Casinos/reducers'
import {combineReducers} from "redux";

export interface IState {
    casinos: Casinos
}

// tslint:disable-next-line
export type TodosAction = ActionType<typeof actionsCasino>;

const rootReducer = combineReducers<IState, TodosAction>({
    casinos: casinoReducer
});

export default rootReducer;