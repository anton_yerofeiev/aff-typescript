import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Route} from 'react-router'
import {BrowserRouter} from 'react-router-dom';

import getClient from './client'
import Casinos from './containers/Casinos'

import {ApolloProvider} from 'react-apollo';
import createStore from "./store/createStore";
import {StoreType} from './store/createStore'
import Container from "./containers/Container";

const store = createStore;

interface ISFCCounterProps {
    innerStore: StoreType
}

const Root: React.SFC<ISFCCounterProps> = ({innerStore}) => (
    <ApolloProvider client={getClient()}>
        <Provider store={innerStore}>
            <BrowserRouter>
                <Container/>
            </BrowserRouter>
        </Provider>
    </ApolloProvider>
);

ReactDOM.render(
    <Root innerStore={store}/>,
    document.getElementById('root'),
);
