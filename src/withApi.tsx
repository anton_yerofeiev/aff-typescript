import * as React from 'react';

interface IOptions  {
    callback: string
}

const withApi = ({callback}: IOptions ) => (Component: React.ComponentType) => {
    class WithApi extends React.Component {
        public async componentDidMount () {
            await this.props[callback]();
        }

        public render() {

            return <Component {...this.props} />;
        }
    }

    return WithApi;
};

export default withApi;