import * as React from "react";

import ContainerComponent from "../../components/Container";

class Container extends React.Component {
    
    public render() {
        return (
            <ContainerComponent />
        )
    }
}

export default Container;