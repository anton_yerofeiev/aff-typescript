import gql from "graphql-tag";
import client from '../../client';
import LoginMutation from './graphql/mutation/Login.graphql';
import {Login} from './graphql/mutation/Login'

export function fetchCasinos(params: Login) {
    return client()
        .mutate({
            mutation: gql(LoginMutation),
            variables: params,
        });
}