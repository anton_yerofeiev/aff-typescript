/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: Login
// ====================================================

export interface Login {
  LogIn: string | null;
}

export interface LoginVariables {
  email: string;
  password: string;
}
