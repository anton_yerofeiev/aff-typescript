/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: Register
// ====================================================

export interface Register {
  SignUp: string | null;
}

export interface RegisterVariables {
  name: string;
  email: string;
  password: string;
}
