import * as React from 'react';
import {Link} from "react-router-dom";

const Header = () => {
    const linkStyle = {
        display: 'inline-block',
        marginRight: '20px'
    }
    return (
        <header style={
            {
                display: 'flex'
            }
        }>
            <nav>
                <ul>
                    <li style={linkStyle}><Link to="/login">Login</Link></li>
                    <li style={linkStyle}><Link to="/test">test</Link></li>
                    <li style={linkStyle}><Link to="/">Home</Link></li>
                </ul>
            </nav>


        </header>

    )
};

export default Header;