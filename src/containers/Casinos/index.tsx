import {connect, MapDispatchToPropsFunction} from 'react-redux';
import {casinosSelector, sortedCasinosSelector} from './selectors'
import {CASINOS_FETCH_REQUESTED} from './constants'
import {compose, withHandlers} from 'recompose';

import withApi from '../../withApi'
import Casinos from '../../components/Casinos'
import {IState} from "../../store/rootReducer";
import {casinosQuery} from "./graphql/query/casinosQuery";

export interface ICasinos {
    casinos: casinosQuery['casinos'];
    sortedCasinos: casinosQuery['casinos']
}

export interface IDispatchProps {
    getCasino: () => void;
}

const mapStateToProps = (state: IState) => ({
    casinos: casinosSelector(state),
    sortedCasinos: sortedCasinosSelector(state)
});


const mapDispatchToProps: MapDispatchToPropsFunction<IDispatchProps, any> = dispatch => ({
    getCasino: () => (dispatch({type: CASINOS_FETCH_REQUESTED}))
});


export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withApi({
        callback: 'getCasino'
    }),
    // withHandlers({
    //     onSort: (props) => (field) => {
    //         props.sortCasinoBy(field);
    //     }
    // })
)(Casinos)