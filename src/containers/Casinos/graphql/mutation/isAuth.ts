/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: isAuth
// ====================================================

export interface isAuth {
  IsAuthMutation: string | null;
}
