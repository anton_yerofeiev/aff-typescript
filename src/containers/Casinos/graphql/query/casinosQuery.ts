/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: casinosQuery
// ====================================================

export interface casinosQuery_casinos {
  __typename: "Casino";
  /**
   * The id of a task
   */
  id: number;
  /**
   * The title of a task
   */
  post_title: string;
}

export interface casinosQuery {
  casinos: (casinosQuery_casinos | null)[] | null;
}
