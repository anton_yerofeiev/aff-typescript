import { createSelector } from 'reselect';
import {IState} from '../../store/rootReducer'

export const casinosSelector = (state: IState) => state.casinos;

export const sortedCasinosSelector = createSelector(
    casinosSelector,
    (casinos) => casinos
);