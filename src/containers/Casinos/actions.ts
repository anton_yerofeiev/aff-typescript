import {Casinos} from "./reducers";


export const getCasinos = (data: Casinos) => ({ type: "GET_CASINOS", payload: data });

export type getCasinoAction = ReturnType<typeof getCasinos>
