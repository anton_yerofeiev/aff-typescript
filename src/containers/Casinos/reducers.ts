import {CASINOS_FETCH_SUCCEEDED} from "./constants";
import {casinosQuery_casinos} from './graphql/query/casinosQuery'
import {getCasinoAction} from './actions'

type Actions = getCasinoAction;

export type Casinos = ReadonlyArray<casinosQuery_casinos>

const initialState: Casinos = [];

const reducer = (state = initialState, action: Actions) => {
    switch (action.type) {
        case CASINOS_FETCH_SUCCEEDED:
            return [ ...state, ...action.payload ];
        default:
            return state;
    }
};

export default reducer;