import gql from "graphql-tag";
import client from '../../client';
import query from './graphql/query/casinosQuery.graphql';
import {casinosQuery_casinos} from './graphql/query/casinosQuery'

export function fetchCasinos() {
    return client()
        .query({
            query: gql(query)
        })
        .then((result: casinosQuery_casinos) => result);
}