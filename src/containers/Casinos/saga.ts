import { call, put, takeLatest } from 'redux-saga/effects'
import {fetchCasinos} from './api'
import {CASINOS_FETCH_SUCCEEDED, CASINOS_FETCH_REQUESTED} from './constants';
import {getCasinoAction} from './actions'

// worker Saga: будет запускаться на экшены типа `USER_FETCH_REQUESTED`
function* fetchUser(action: getCasinoAction) {
    try {
        const data = yield call(fetchCasinos, action.payload);
        yield put({type: CASINOS_FETCH_SUCCEEDED, payload: data.data.casinos.edges});
    } catch (e) {
        yield put({type: "CASINOS_FETCH_FAILED", message: e.message});
    }
}

function* mySaga() {
    yield takeLatest(CASINOS_FETCH_REQUESTED, fetchUser);
}

export default mySaga;