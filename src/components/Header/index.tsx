import * as React from 'react';
import {Link} from "react-router-dom";
import MainMenu from "./MainMenu";
import PrimaryMenu from "./PrimaryMenu";
import AuthorizationBox from "./AuthorizationBox";
import InputWithPlaceholder from "../InputWithPlaceholder";
import SearchForm from "./SearchForm";



const Header = () => {
    const linkStyle = {
        display: 'inline-block',
        marginRight: '20px'
    }
    return (
        <header className="header">
            <div className="header__navigationTop navigationTop " style={{flexWrap: 'wrap'}}>

                <MainMenu/>
                <PrimaryMenu/>
                <AuthorizationBox/>
                <div className="container">
                    <div className="header__middle" id="header">
                        <div className="header__row">
                            <div className="header__logo-link">
                                <a href="/" className=""
                                   title="">
                                    <img className=""
                                         src="https://affgambler.ru/wp-content/themes/sportsnews/images/logo-affgambler.svg"
                                         alt=""/>
                                </a>
                            </div>

                            <a href="#" className="mobile-menu js-mobile-menu"><span className="line-1"/></a>
                            <div className="mobile-menu-over js-mobile-menu-over"/>

                            <SearchForm guess="searchGuess"/>

                        </div>
                    </div>
                </div>
            </div>

        </header>

    )
};

export default Header;