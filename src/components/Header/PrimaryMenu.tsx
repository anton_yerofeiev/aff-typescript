import * as React from 'react';
import {Link} from "react-router-dom";


export default class PrimaryMenu extends React.Component {
    public render() {
        return (
            <ul id="primary-menu" className="navigationTop__menu">
                <li id="menu-item-2029"
                    className="menu-item menu-item-type-post_type menu-item-object-page"><a
                    title="ТОП рейтинг онлайн казино" href="/casino-rating/">Рейтинг казино</a>
                </li>
                <li id="menu-item-2087"
                    className="menu-item menu-item-type-post_type menu-item-object-page"><a
                    title="Претензии и жалобы на онлайн казино"
                    href="/casino-complaints/">Жалобы</a></li>
                <li id="menu-item-13663"
                    className="menu-item menu-item-type-custom menu-item-object-custom"><a
                    title="Все бонусы казино" href="/bonus/">Бонусы</a></li>
                <li id="menu-item-2089"
                    className="menu-item menu-item-type-post_type menu-item-object-page"><a
                    title="Новые игровые автоматы бесплатно" href="/all-free-slots/">Игровые
                    автоматы</a></li>
                <li id="menu-item-27"
                    className="menu-item menu-item-type-post_type menu-item-object-page"><a
                    title="Твич стримы казино онлайн" href="/streams/">Стримы <span
                    className="point_stream"/></a></li>
                <li id="menu-item-1887"
                    className="menu-item menu-item-type-post_type menu-item-object-page"><a
                    title="Видео игры в казино, лудоводы"
                    href="/video-casino-online/">Видео</a></li>
                <li id="menu-item-21752"
                    className="menu-item menu-item-type-custom menu-item-object-custom"><a
                    title="Последние новости казино" href="/casino-news/">Новости</a></li>
                <li id="menu-item-21753"
                    className="menu-item menu-item-type-custom menu-item-object-custom"><a
                    title="Онлайн казино форум" href="/forum/">Форум</a></li>
            </ul>
        )
    }
}