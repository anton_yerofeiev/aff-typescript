import * as React from 'react';
import InputWithPlaceholder from "../InputWithPlaceholder";
import {Form, Text} from 'react-form';
import {SyntheticEvent} from "react";


export default class SearchForm extends React.Component<any, any> {
    private formRef: React.RefObject<HTMLFormElement>;

    constructor(props: any) {
        super(props);

        this.formRef = React.createRef();

        this.state = {
            showPlaceholder: true
        }
    }

    public render() {
        const {guess} = this.props;
        return (

            <Form onSubmit={this.onSubmitHandler}>
                {formApi => (
                    <form ref={this.formRef} className="search-form" onSubmit={formApi.submitForm}>
                        <InputWithPlaceholder guess={guess} />
                        <Text className="search-input main-search" />
                        <button type="submit" className="search-input-button" hidden={true} value=""/>
                        <div className="search-tags ">
                            <button type="button">Везде</button>
                            <button type="button">Онлайн казино</button>
                            <button type="button">Бонусы</button>
                            <button type="button">Слоты</button>
                            <button type="button">Статьи</button>
                            <button type="button">Блоги</button>
                            <button type="button">Жалобы</button>
                        </div>
                        <div className="search-result" aria-hidden="true"/>
                    </form>
                )}
            </Form>
        )
    }

    private onSubmitHandler = (values: any) => {
        // event.preventDefault();
        console.log(values)
    }
}