import * as React from 'react';
import {Link} from "react-router-dom";


export default class MainMenu extends React.Component {
    public render() {
        return (
            <>
                <button className="header__toggle js-toggle-menu" id="toggleMainMenu" aria-label="Toggle main menu"
                        aria-haspopup="true" aria-expanded="false">
                    <span className="header__hamburger hamburger"><span className="hamburger__item"/></span>
                    Разделы
                </button>
                <div className="header__mainMenu mainMenu" aria-labelledby="toggleMainMenu" aria-hidden="true">
                    <div className="mainMenu__wrapper">
                        <div className="mainMenu__container">
                            <div className="mainMenu__col">
                                <ul className="mainMenu__list">
                                    <li className="mainMenu__item">
                                        <svg className="icons star">
                                            <use tabIndex={-1}
                                                 xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#star"/>
                                        </svg>
                                        <a href="/casino-rating/" className="mainMenu__link">Рейтинг казино</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons usertyping">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#usertyping"/>
                                        </svg>
                                        <a href="/casino-complaints/" className="mainMenu__link">Жалобы</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons smallthumbnails">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#smallthumbnails"/>
                                        </svg>
                                        <a href="/bonus/" className="mainMenu__link">Бонусы</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons play">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#play"/>
                                        </svg>
                                        <a href="/all-free-slots/" className="mainMenu__link">Игровые автоматы</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons chat">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#chat"/>
                                        </svg>
                                        <a href="/reviews/" className="mainMenu__link">Отзывы игроков</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons gopro">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#gopro"/>
                                        </svg>
                                        <a href="/streams/" className="mainMenu__link">Стримы</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons video">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#video"/>
                                        </svg>
                                        <a href="/video-casino-online/" className="mainMenu__link">Видео</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="mainMenu__col">
                                <ul className="mainMenu__list">
                                    <li className="mainMenu__item">
                                        <svg className="icons list">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#list"/>
                                        </svg>
                                        <a href="/casino-news/" className="mainMenu__link">Новости</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons edit">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#edit"/>
                                        </svg>
                                        <a href="/blog/" className="mainMenu__link">Блог</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons certificate">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#certificate"/>
                                        </svg>
                                        <a href="/sertifitsirovannye-kazino-onlajn/" className="mainMenu__link">Сертиф.
                                            казино</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons bulletedlist">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#bulletedlist"/>
                                        </svg>
                                        <a href="/new-casino/" className="mainMenu__link">Новые казино</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons warning">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#warning"/>
                                        </svg>
                                        <a href="/blacklisted-casinos/" className="mainMenu__link">Черный список</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons standby">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#standby"/>
                                        </svg>
                                        <a href="/closed-casinos/" className="mainMenu__link">Закрытые казино</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons book">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#book"/>
                                        </svg>
                                        <a href="/slovar-terminov/" className="mainMenu__link">Словарь терминов</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="mainMenu__col">
                                <ul className="mainMenu__list">
                                    <li className="mainMenu__item">
                                        <svg className="icons codetags">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#codetags"/>
                                        </svg>
                                        <a href="/software/" className="mainMenu__link">Софт</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons exchange">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#exchange"/>
                                        </svg>
                                        <a href="/banking/" className="mainMenu__link">Методы депозита</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons profile">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#profile"/>
                                        </svg>
                                        <a href="/license/" className="mainMenu__link">Лицензии</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons dollar">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#dollar"/>
                                        </svg>
                                        <a href="/currencies/" className="mainMenu__link">Валюты</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <svg className="icons location">
                                            <use
                                                xlinkHref="/wp-content/themes/sportsnews/images/spritesSymbol.svg#location"/>
                                        </svg>
                                        <a href="/languages/" className="mainMenu__link">Языки</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="mainMenu__col mainMenu__col_wide">
                                <ul className="mainMenu__list">

                                    <li className="mainMenu__item">
                                        <a href="/no-deposit-casino-bonuses/" className="mainMenu__link">Бездепозитные
                                            бонусы
                                            казино</a>
                                        <span className="mainMenu__note">Список казино с бездепозитным бонусом за регистрацию</span>
                                    </li>
                                    <li className="mainMenu__item">
                                        <a href="/welcome-bonuses/" className="mainMenu__link">Приветственые бонусы</a>
                                        <span
                                            className="mainMenu__note">Казино, предоставляющие  бонус на первый депозит</span>
                                    </li>
                                    <li className="mainMenu__item">
                                        <a href="/free-spins/" className="mainMenu__link">Бесплатные вращения (Free
                                            Spins)</a>
                                        <span
                                            className="mainMenu__note">Бесплатные вращения за регистрацию в казино</span>
                                    </li>
                                    <li className="mainMenu__item">
                                        <a href="/bonus-codes-for-online-casinos/" className="mainMenu__link">Бонус коды
                                            казино</a>
                                    </li>
                                    <li className="mainMenu__item">
                                        <a href="/online-casino-cashback-bonuses/" className="mainMenu__link">Кэшбэк
                                            бонусы
                                            казино</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="mainMenu__bottom">
                                <a href="/razdely-sajta/" className="mainMenu__all">Все разделы</a>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}