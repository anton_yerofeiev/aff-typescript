import * as React from 'react';
import Header from "../Header";
import Content from "../../containers/Content";

const ContainerComponent = () => (
    <div className="wrapper">
        <>
            <Header/>
            <Content/>
        </>
    </div>
);

export default ContainerComponent;