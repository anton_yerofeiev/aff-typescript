import * as React from 'react';
import { Text } from 'react-form';

interface IMainProps {
    guess: string;
}

interface IMainState {
    showPlaceholder: boolean;
}

export default class InputWithPlaceholder extends React.Component<IMainProps, IMainState> {

    constructor(props: IMainProps) {
        super(props);

        this.state = {
            showPlaceholder: true
        }
    }

    public render() {
        const {guess} = this.props;
        const {showPlaceholder} = this.state;
        return (
            <div className="search-input-wr ">
                <Text
                    type="text"
                    id="search-header"
                    placeholder=""
                    className="search-input main-search"
                    onInput={this.onInputHandler}
                    onBlur={this.onInputHandler}
                    name="searchField"
                />
                <label style={{display: showPlaceholder ? 'block' : 'none'}} className="search-placeholder " htmlFor="search-header">Введите поисковый
                    запрос. Например <a href="/"
                                        className="search-guess">{guess}</a></label>

            </div>
        )
    }

    private onInputHandler = (event: React.SyntheticEvent) => {
        const target = event.target as HTMLInputElement;

        this.setState({
            showPlaceholder: target.value.length === 0
        })

    };
}