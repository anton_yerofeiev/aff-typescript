import * as React from 'react';

interface IProps  {
    bonus: {
        value: string,
        title: string,
        description: string,
    },
    type: string
}

const Popover = ({bonus, type}: IProps) => {
    console.log(bonus);
    return (
        <React.Fragment>
            <span className="popover">{bonus.value}</span>
            <div className="popoverBox"><span className="popoverBox__title">{bonus.title}</span>
                <span className="popoverBox__bonus">{bonus.value}</span>
                <div className="popoverBox__text">{bonus.description}</div>
            </div>
        </React.Fragment>

    )
};

export default Popover;