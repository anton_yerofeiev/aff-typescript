import {ICasinos, IDispatchProps} from "../../containers/Casinos";
import {translate} from "../../utils/helpers";
import Rating from '../Rating';
import Popover from "../Popover";
import classnames from 'classnames';
import * as React from 'react';


type Props = ICasinos & IDispatchProps;

const Casinos: React.StatelessComponent<Props> = (props) => {
    return (
        <table className="table tableGeneral">
            <thead>
            <tr>
                {/*<th/>*/}
                {/*<th onClick={() => props.onSort('title')}>Title</th>*/}
                {/*<th onClick={() => props.onSort('rating')}>Rating</th>*/}
                {/*<th onClick={() => props.onSort('bonus')}>Bonuses</th>*/}
                {/*<th onClick={() => props.onSort('review')}>Rating</th>*/}
                {/*<th/>*/}
            </tr>
            </thead>
            <tbody>
            {
                props.sortedCasinos && props.sortedCasinos.map((item, index) => {
                    const casino = item;
                    // const link = casino.link.replace('https://affgambler.local', '');
                    const playButtonClass = classnames({
                        'btn btn_blue btn--play': true,
                        'btn--first': index < 3,
                    });
                    return (

                    <tr key={casino.id} className="odd">
                        <td className="table-number"><span className="number">{++index}</span></td>
                        <td>
                            {/*<a href={link} className="casino-link">*/}
                            {/*<img src={casino.image} alt={casino.title}/>*/}
                                <span>{casino.post_title}</span>
                            {/*</a>*/}
                        </td>
                        <td className="table-rating">
                            {/*<span className="rate-number">{casino.rating}</span>*/}
                            {/*<Rating rating={casino.rating} />*/}
                        </td>
                        <td className="table-Pbonus">
                            {/*<Popover bonus={JSON.parse(casino.bonus)} />*/}
                        </td>
                        <td className="table-reviews">
                            {/*<a*/}
                            {/*href={link}>{casino.countReviews} {translate(+casino.countReviews, ['отзыв', 'отзыва', 'отзывов'])} </a>*/}
                        </td>
                        <td className="table-more"><a target="_BLANK" href="/click/casinox"
                                                      className={playButtonClass}>Играть</a></td>
                    </tr>
                )})
            }
            </tbody>
        </table>
    )
};


export default Casinos;