import * as React from 'react';
import {times} from 'lodash';

interface IProps {
    rating: number
}

const Rating = ({rating}: IProps) => {
    let lastActive = 0;
    rating = rating <= 10 ? rating : 10;
    return (
        <ul className="rate rate-active">
            {times(5, i => {
                const isActive = rating / (i + 1) / 2 >= 1;
                lastActive = isActive ? i : lastActive;
                return (
                    <li key={i} className={`c-rating__item${isActive ? ' is-active' : ''}`}>
                        {!isActive && lastActive + 1 === i && (
                            <span style={{width: ((rating / 2) - i) * 100 + '%'}}/>
                        )}
                    </li>)
            })}
        </ul>
    )
};

export default Rating;