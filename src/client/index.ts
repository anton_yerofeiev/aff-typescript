import {InMemoryCache} from 'apollo-cache-inmemory';
import {ApolloClient} from 'apollo-client';
import {HttpLink} from 'apollo-link-http';

let client: any = null;

const getClient = () => {
    if (!client) {
        client = new ApolloClient({
            cache: new InMemoryCache(),
            link: new HttpLink({uri: 'https://affgambler.local/graphql'}),
        })
    }
    return client;
}

export default getClient;